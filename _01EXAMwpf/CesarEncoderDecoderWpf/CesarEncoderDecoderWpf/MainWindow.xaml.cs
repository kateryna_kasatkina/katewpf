﻿using DLL;
using Microsoft.Win32;
using System.IO;
using System.Text;
using System.Windows;

namespace CesarEncoderDecoderWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static int counter = 0;
        private static int step = 3;
        private static string path = @"c:\MyData\test.txt";
        private static CesarEncoder cesarEndoder = new CesarEncoder(step);
        private static CesarDecoder cesarDecode = new CesarDecoder(step);
        private static MyTxtLogger logger = new MyTxtLogger();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnOpenFDlg1_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                string fullname = openFileDialog.FileName;
                txtFileNameODlg1.Text = fullname;
                OriginaNoEncoded.Text = File.ReadAllText(fullname);
                // OriginaNoEncoded.Text = File.ReadAllText(fullname, Encoding.GetEncoding(1251));
                logger.WriteProtocol(counter, "Get data for encoding", File.ReadAllText(fullname));
            }
           
        }

        private void btnOpenFDlg2_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                string fullname = openFileDialog.FileName;
                txtFileNameODlg2.Text = fullname;
                OriginaEncoded.Text = File.ReadAllText(fullname);
                //OriginaEncoded.Text = File.ReadAllText(fullname, Encoding.GetEncoding(1251));
                logger.WriteProtocol(counter, "Get data for decoding", File.ReadAllText(fullname));
            }
        }

        private void btnEncode_Click(object sender, RoutedEventArgs e)
        {
            cesarEndoder.Encode(path);
            EncodingResultTxtBlock.Text = File.ReadAllText(path);

        }

        private void btnDecode_Click(object sender, RoutedEventArgs e)
        {
            cesarDecode.Decode(path);
            DecodingResultTxtBlock.Text = File.ReadAllText(path);
           
        }
    }
}

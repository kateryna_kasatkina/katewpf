﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL
{
   public  class CesarDecoder
    {
        private int _step;
        public int Step
        {
            get { return _step; }
            set { _step = value; }
        }
        private CesarDecoder() { }
        public CesarDecoder(int aStep)
        {
            this.Step = aStep;
        }
        public void Decode(string nameFile)
        {
            string text = String.Empty;
            try
            {
                using (StreamReader sr = new StreamReader(nameFile, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            using (FileStream fstream = new FileStream(nameFile, FileMode.OpenOrCreate))
            {
                byte[] output = Encoding.Default.GetBytes(text);
                for (int i = 0; i < output.Length; i++)
                {
                    Console.Write((char)output[i] + " " + output[i] + "-");
                    output[i] -= (byte)Step;
                    Console.WriteLine((char)output[i] + " " + output[i]);
                }
                string textFromFile = Encoding.Default.GetString(output);
                fstream.Close();
                try
                {
                    using (StreamWriter sw = new StreamWriter(nameFile, false, System.Text.Encoding.Default))
                    {
                        sw.WriteLine(textFromFile);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Console.WriteLine("Текст записан в файл");
            }
        }



        public void DecodeRu(string nameFile)
        {
            string text = String.Empty;
            try
            {
                using (StreamReader sr = new StreamReader(nameFile, System.Text.Encoding.GetEncoding(1251)))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            using (FileStream fstream = new FileStream(nameFile, FileMode.OpenOrCreate))
            {
                byte[] output = Encoding.GetEncoding(1251).GetBytes(text);
                for (int i = 0; i < output.Length; i++)
                {
                    Console.Write((char)output[i] + " " + output[i] + "-");
                    output[i] -= (byte)Step;
                    Console.WriteLine((char)output[i] + " " + output[i]);
                }
                string textFromFile = Encoding.GetEncoding(1251).GetString(output);
                fstream.Close();
                try
                {
                    using (StreamWriter sw = new StreamWriter(nameFile, false, System.Text.Encoding.GetEncoding(1251)))
                    {
                        sw.WriteLine(textFromFile);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                Console.WriteLine("Текст записан в файл");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadraticEquationTest
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder log = new StringBuilder();
            TestIfDLessZero(log);
            TestIfDGreaterZero(log);
            TestIfDEqualsZero(log);

            Console.WriteLine(log.ToString());
            Console.WriteLine("Press any key");
            Console.ReadKey();
        }


        public static void TestIfDGreaterZero(StringBuilder log)
        {
            QuadraticEquation qa = new QuadraticEquation(1, -7, 12);

            double x1;
            double x2;
            bool hasSolution = qa.calc(out x1, out x2);
            if (hasSolution == true)
            {
                log.Append("D>0");
                if ((x1 == -0.5) && (x2 == -0.5))
                {
                    log.Append("Test ok");
                }
                log.AppendFormat("x1={0},x2={1}", x1, x2);
            }
        }

        public static void TestIfDEqualsZero(StringBuilder log)
        {
            QuadraticEquation qa = new QuadraticEquation(4, 4, 1);

            double x1;
            double x2;
            bool hasSolution = qa.calc(out x1, out x2);
            if (hasSolution == true)
            {
                log.Append("D==0");
                if ((x1 == -0.5) && (x2 == -0.5))
                {
                    log.Append("Test ok");
                }
                log.AppendFormat("x1={0},x2={1}", x1, x2);
            }
        }

        public static void TestIfDLessZero(StringBuilder log)
        {
            QuadraticEquation qa = new QuadraticEquation(4, 4, 1);

            double x1;
            double x2;
            bool hasSolution = qa.calc(out x1, out x2);
            if (hasSolution == false)
            {
                log.Append("Корней нет.D<0");
            }
        }
    }
}

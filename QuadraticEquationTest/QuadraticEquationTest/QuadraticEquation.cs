﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadraticEquationTest
{
    public class QuadraticEquation
    {
        private double a;
        private double b;
        private double c;

        public QuadraticEquation(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        internal bool calc(out double x1, out double x2)
        {
            bool result = false;
            double d = b * b - 4 * a * c;
            x1 = 0;
            x2 = 0;
            if(d>=0)
            {
                x1 = (-b - Math.Sqrt(d)) / (2 * a);
                x1 = (-b + Math.Sqrt(d)) / (2 * a);
            }
            return result;
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathHelperTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void sum_10_and_20_30_returned()
        {
            /*
             Алгоритм 

             1.0 Готовлю тестовые данные
             2.0 Создаю тест, который использует MathHelper
             3.0 Проверяю результат при помощи утверждений
             4.0 Утверждение

             */
            int A = 10;
            int B = 20;
            int expected = 30;

            MathsHelper helper = new MathsHelper();
            int result = helper.Add(A, B);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void dif_20_and_10_10_returned()
        {
            /*
             Алгоритм 

             1.0 Готовлю тестовые данные
             2.0 Создаю тест, который использует MathHelper
             3.0 Проверяю результат при помощи утверждений
             4.0 Утверждение

             */
            int A = 20;
            int B = 10;
            int expected = 20;
            //2
            MathsHelper helper = new MathsHelper();
            int result = helper.Subtract(A, B);
            Assert.AreEqual(expected, result);
        }
    }
}

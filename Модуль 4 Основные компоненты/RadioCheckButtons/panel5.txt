 <StackPanel Margin="10" x:Name="panel5">
                <Label FontWeight="Bold">Application Options</Label>
                <CheckBox>
                    <TextBlock>
                                Enable feature <Run Foreground="Green" FontWeight="Bold">ABC</Run>
                    </TextBlock>
                </CheckBox>
                <CheckBox IsChecked="True">
                    <WrapPanel>
                        <TextBlock>
                                        Enable feature <Run FontWeight="Bold">XYZ</Run>
                        </TextBlock>
                        <Image Source="pics/bullet_green.png"  Width="16" Height="16" Margin="5,0" />
                    </WrapPanel>
                </CheckBox>
                <CheckBox>
                    <TextBlock>
                                Enable feature <Run Foreground="Blue" TextDecorations="Underline" FontWeight="Bold">WWW</Run>
                    </TextBlock>
                </CheckBox>
            </StackPanel>

<StackPanel Margin="10" x:Name="panel3">
                <ComboBox Name="cmbColors">
                    <ComboBox.ItemTemplate>
                        <DataTemplate>
                            <StackPanel Orientation="Horizontal">
                                <Rectangle Fill="{Binding Name}" Width="16" Height="16" Margin="0,2,5,2" />
                                <TextBlock Text="{Binding Name}" />
                            </StackPanel>
                        </DataTemplate>
                    </ComboBox.ItemTemplate>
                </ComboBox>
            </StackPanel>
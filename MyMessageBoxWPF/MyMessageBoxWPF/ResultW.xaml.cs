﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyMessageBoxWPF
{
    /// <summary>
    /// Interaction logic for ResultW.xaml
    /// </summary>
    public partial class ResultW : Window

    {
        string YesNo;
        string Check;
        public ResultW(string yesNo, string check)
        {
            InitializeComponent();
            tb1.Text = check + "   ответ:  " + yesNo;
        }
    }
}

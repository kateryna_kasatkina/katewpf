﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyMessageBoxWPF
{
    /// <summary>
    /// Interaction logic for QuestionW.xaml
    /// </summary>
    public partial class QuestionW : Window
    {
        public QuestionW(string text)
        {
            InitializeComponent();
            tbQuestion.Text = text;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string YesNo = "Не определено";
            string check = String.Empty;

            if (rbOK.IsChecked == true)
            {
                YesNo = "ДА";

            }
            else
                YesNo = "НЕТ";

            if (checkBox1.IsChecked == true)
            {
                check = "Вы  согласились с условием";
                ResultW rw = new ResultW(YesNo, check);
                rw.ShowDialog();
            }
            
        }
    }
}

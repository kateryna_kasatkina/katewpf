﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Windows;

namespace SimpleBindingWPFMsSql
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            GetDataFromDataBase();
            testTextBlockFilling();
            testListBoxFilling();
            testComboBoxFilling();

        }
        private List<Department> Departments = new List<Department>();
        private List<Employee> Employees = new List<Employee>();
        private Dictionary<Department, IEnumerable<Employee>> GroupEmployee;
        private DataSet data;
        
        private void testComboBoxFilling()
        {
            this.myComboBox.ItemsSource = this.data.Tables["Departments"].AsDataView();

            DataView dv = (DataView)this.myComboBox.ItemsSource;
            dv.Sort = "Departments_ID";
            DataRow[] rows = this.data.Tables["Departments"].Select("Departments_ID=2");
            DataRowView[] rowsView = dv.FindRows(rows[0]["Departments_ID"]);
            this.myComboBox.SelectedItem = rowsView[0];

            //this.myComboBox.SelectedIndex = 1;
        }

        private void testListBoxFilling()
        {
            List<TodoItem> items = new List<TodoItem>();
            items.Add(new TodoItem() { Title = "Complete this WPF tutorial", Completion = 45 });
            items.Add(new TodoItem() { Title = "Learn C#", Completion = 80 });
            items.Add(new TodoItem() { Title = "Wash the car", Completion = 0 });

            //lbTodoList.ItemsSource = items;


            lvTodoList1.ItemsSource = this.data.Tables["Departments"].AsDataView();


            lvTodoList2.ItemsSource = this.data.Tables["Departments"].AsDataView();
            lvTodoList2.SelectedValuePath = "Departments_ID";/*!должно совпадать с названием в базе данных*/
            lvTodoList2.DisplayMemberPath = "DEPARTMENTS_NAME";

            lvTodoList3.ItemsSource = this.data.Tables["Departments"].AsDataView();

            lvTodoList4.ItemsSource = this.data.Tables["Departments"].AsDataView();


        }
        public static DataSet GetCustomers()
        {
            // query
            string sql = @"
                SELECT *
                FROM Customers
             ";

            // create connection
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "Customers");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
        private void testTextBlockFilling()
        {
            List<TextViewer> list = new List<TextViewer>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 100; i++)
            {
                list.Add(new TextViewer(String.Format("{0},{1}", i.ToString(), "Some text...")));
                sb.AppendLine(list[i].DisplayText);
            }

            TextViewer tv = new TextViewer(sb.ToString());
            this.txtMyTextBox.DataContext = tv;
        }

        private void GetDataFromDataBase()
        {
            try
            {
                // Specify a connection string. Replace the given value with a 
                // valid connection string for a Northwind SQL Server sample
                // database accessible to your system.
                var connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
                SqlConnection connection =
                    new SqlConnection(connectionString);
                connection.Open();

                // Create a DataSet.
                this.data = new DataSet();
                data.Locale = System.Globalization.CultureInfo.InvariantCulture;

                // Add data from the Customers table to the DataSet.
                SqlDataAdapter masterDataAdapter = new
                    SqlDataAdapter("select * from Departments", connection);
                masterDataAdapter.Fill(data, "Departments");

                // Add data from the Orders table to the DataSet.
                SqlDataAdapter detailsDataAdapter = new
                    SqlDataAdapter("select * from Employees", connection);
                detailsDataAdapter.Fill(data, "Employees");

                // Establish a relationship between the two tables.
                DataRelation relation = new DataRelation("EmployeesDepartments",
                    data.Tables["Departments"].Columns["Departments_ID"],
                    data.Tables["Employees"].Columns["Departments_ID"]);
                data.Relations.Add(relation);

                // Bind the master data connector to the Customers table.
                //masterBindingSource.DataSource = data;
                //masterBindingSource.DataMember = "Departments";

                //DataView dv = data.Tables["Departments"].DefaultView;
                //dv.RowFilter = "DepartmentsLocation = 'Kharkiv'";
                //dv.Sort = "DepartmentsLocation";

                //dv = new DataView(data.Tables["Departments"], "DepartmentsLocation = 'London'", "DepartmentsLocation", DataViewRowState.CurrentRows);
                //Create new table based on filtered records
                // DataTable newTable = dv.ToTable("BrazilianContactNames", true, new string[] { "ContactName" });
                /*
                masterBindingSource.DataSource = dv;
                // Bind the details data connector to the master data connector,
                // using the DataRelation name to filter the information in the 
                // details table based on the current row in the master table. 
                detailsBindingSource.DataSource = masterBindingSource;
                detailsBindingSource.DataMember = "EmployeesDepartments";
                */

            }
            catch (SqlException ex)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system. : " + ex.ToString());
            }
        }
    }
}


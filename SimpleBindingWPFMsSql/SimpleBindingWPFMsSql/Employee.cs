﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBindingWPFMsSql
{
    public class Employee
    {
        private int employeeNo;
        private String employeeName;
        private int deptNo;
        private String job;
        private Double salary;

        public Employee() { }

        public Employee(int EmployeeNo, String EmployeeName, int DeptNo, String Job, Double Salary)
        {
            employeeNo = EmployeeNo;
            employeeName = EmployeeName;
            deptNo = DeptNo;
            job = Job;
            salary = Salary;
        }
        public int EmployeeNo
        {
            set { employeeNo = value; }
            get { return employeeNo; }
        }
        public String EmployeeName
        {
            set { employeeName = value; }
            get { return employeeName; }
        }
        public int DeptNo
        {
            set { deptNo = value; }
            get { return deptNo; }
        }

        public String Job
        {
            set { job = value; }
            get { return job; }
        }

        public Double Salary
        {
            set { salary = value; }
            get { return salary; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBindingWPFMsSql
{
    public class TextViewer
    {
        private String datasource;
        public TextViewer(String source)
        {
            this.datasource = source;
        }

        public string DisplayText
        {
            get
            {
                if (datasource.Contains("<bold>") == false)
                {
                    //return "<bold>" + datasource + "</bold>";
                    return datasource;
                }
                return datasource;
            }
            set { datasource = value; }
        }
    }

}

                <StackPanel.Resources>
                    <Style BasedOn="{StaticResource RectStyle1}" TargetType="Rectangle">
                        <Setter Property="Width" Value="50" />
                        <Setter Property="Height" Value="10" />
                        <Setter Property="Margin" Value="2" />
                        <Setter Property="HorizontalAlignment" Value="Left" />
                        <Style.Triggers>
                            <EventTrigger RoutedEvent="MouseEnter">
                                <BeginStoryboard>
                                    <Storyboard>
                                        <DoubleAnimation To="300" Duration="0:0:1.5" 
                AccelerationRatio="0.10" DecelerationRatio="0.25" 
                Storyboard.TargetProperty="(Canvas.Width)" />
                                    </Storyboard>
                                </BeginStoryboard>
                            </EventTrigger>
                            <EventTrigger RoutedEvent="MouseLeave">
                                <BeginStoryboard>
                                    <Storyboard>
                                        <DoubleAnimation Duration="0:0:1.5" 
                AccelerationRatio="0.10" DecelerationRatio="0.25" 
                Storyboard.TargetProperty="(Canvas.Width)" />
                                    </Storyboard>
                                </BeginStoryboard>
                            </EventTrigger>
                        </Style.Triggers>
                    </Style>
                </StackPanel.Resources>
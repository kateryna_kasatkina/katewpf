﻿using NUnit.Framework;
using NUnit_Application;
namespace Tests
{
    [TestFixture]//класс виден для автоматического запуска тестов
                 //NUnit Adapter-ом
    public class TestsCases
    {
        [Test]
        public void AddTest()
        {
            MathsHelper helper = new MathsHelper();
            int result = helper.Add(20, 10);
            Assert.AreEqual(40, result);
        }
        [TestCase]
        public void SubtractTest()
        {
            MathsHelper helper = new MathsHelper();
            int result = helper.Subtract(20, 10);
            Assert.AreEqual(10, result);
        }
    }
}


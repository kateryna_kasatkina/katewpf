﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConsoleApp.Program.Tests
{
    [TestClass()]
    public class MathsHelperTests
    {
        [TestMethod()]
        public void AddTest()
        {
            //1.Готовим тестовые данные
            int A = 10;
            int B = 20;
            int expected = 30;

            //2.Создаем тест
            MathsHelper mh = new MathsHelper();
            int result = mh.Add(A, B);

            //3.Проверяем выполнение
            Assert.AreEqual(expected,result);
        }

        [TestMethod()]
        public void SubtractTest()
        {
            //1.Готовим тестовые данные
            int A = 20;
            int B = 10;
            int expected = 10;

            //2.Создаем тест
            MathsHelper mh = new MathsHelper();
            int result = mh.Subtract(A, B);

            //3.Проверяем выполнение
            Assert.AreEqual(expected, result);
        }

        public void sum_10_and_20_30_returned()
        {

        }
    }
}
// <copyright file="MathsHelperTest.cs">Copyright ©  2017</copyright>
using System;
using MathHelperTest;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathHelperTest.Tests
{
    /// <summary>This class contains parameterized unit tests for MathsHelper</summary>
    [PexClass(typeof(MathsHelper))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class MathsHelperTest
    {
        /// <summary>Test stub for Add(Int32, Int32)</summary>
        [PexMethod]
        public int AddTest(
            [PexAssumeUnderTest]MathsHelper target,
            int a,
            int b
        )
        {
            int result = target.Add(a, b);
            return result;
            // TODO: add assertions to method MathsHelperTest.AddTest(MathsHelper, Int32, Int32)
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WOFRoutedEventsTEST1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        StringBuilder realLOG = new StringBuilder();
  
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_TextChanged(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Событие TextChanged перехвачено компнентом Window");
            realLOG.Append("Hey from Window--");
            e.Handled = (bool)RadioButton3.IsChecked;
        }

        private void TextBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            MessageBox.Show("Событие TextChanged перехвачено компнентом TextBox");
            realLOG.Append("Hey from TextBox--");
            e.Handled = (bool)RadioButton1.IsChecked;//подавляем дальнейшее "движение" события
        }

        private void Grid_TextChanged(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Событие TextChanged перехвачено компнентом Grid");
            realLOG.Append("Hey from Grid--");
            e.Handled = (bool)RadioButton2.IsChecked;
        }

        private void LOGbtn_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(realLOG.ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;//работа с файловыми потоками (Stream)
using Microsoft.Win32;//работа с системными диалогами
                      //OpenFile, SaveFile, FontDialog, ColorDialog

namespace FileDialogsWpfTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public MainWindow(String[] args)
        {
            InitializeComponent();
            string path = args[args.Length - 1];
            MessageBox.Show("Запущено " + path);
            btnDecode.Content = "Заши-" + Environment.NewLine + "фровать";
        }
        private void btnOpenFDlg_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                string fullname = openFileDialog.FileName;
                txtFileNameODlg.Text = fullname;
                if (File.Exists(fullname))
                    txtOrigTxt.Text = File.ReadAllText(fullname);
            }
        }
        private void btnSaveFDlg_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {
                string fullName = saveFileDialog.FileName;
                txtFileNameSDlg.Text = fullName;
                if (String.IsNullOrEmpty(txtDecodeTxt.Text) == false)
                {
                    File.WriteAllText(fullName, txtDecodeTxt.Text);
                }

            }

        }
        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            AboutWindow about = new AboutWindow();
            about.ShowDialog();
        }
        private void Button_Click_1(object sender, System.Windows.RoutedEventArgs e)
        {
            AboutWindow about = new AboutWindow();
            about.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace AnimationInWPFTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
          
        }

        

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            var da = new DoubleAnimation(360, 0, new Duration(TimeSpan.FromSeconds(1)));
            var rt = new RotateTransform();
            rect2.RenderTransform = rt;
            rect2.RenderTransformOrigin = new Point(0.5, 0.5);
            da.RepeatBehavior = RepeatBehavior.Forever;
            rt.BeginAnimation(RotateTransform.AngleProperty, da);
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            // Storyboard находится в секции Resources у Windows
            Storyboard s = (Storyboard)TryFindResource("sb");//sb - имя компонента в ресурсах
            s.Begin();	// Start animation
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            Storyboard s = (Storyboard)TryFindResource("sb");
            s.Stop();	// Stop animation
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            Storyboard s = (Storyboard)TryFindResource("sb");
            s.Pause();	// Pause animation
        }

        private void btnResume_Click(object sender, RoutedEventArgs e)
        {

            Storyboard s = (Storyboard)TryFindResource("sb");
            s.Resume();	// Resume animation
        }

        private void myBtn_Click(object sender, RoutedEventArgs e)
        {
            DoubleAnimation myDoubleAnimation = new DoubleAnimation();
            myDoubleAnimation.From = 1.0;
            myDoubleAnimation.To = 0.0;
            myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));

            //myDoubleAnimation.AutoReverse = true;
            //myDoubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
        }

        private void btnDisappear_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
